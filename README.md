# Singularity image for phASEr

- Dependencies are installed through conda (see `files/environment.yml`)
    - The conda environment should be automatically activated. If not :
    `. /usr/local/etc/profile.d/conda.sh`
    `conda activate phaser_deps`
- PhASEr is installed in `/opt/src/phaser/`
- Bash wrapper scripts to call the phASEr commands are stored in `/usr/bin`


